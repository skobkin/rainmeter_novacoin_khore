function Initialize()

-- Set pointers to the two measures we want to manipulate in the script.
-- Only need to do this once, so we do it in Initialize().
msNvc    = SKIN:GetMeasure("MeasureBalance") 
msNvcUsd  = SKIN:GetMeasure("measureNVCUSD")
--msUsdBalance = SKIN:GetMeter("MeterUSD")

end -- function Initialize

function Update()

   sNvc     = msNvc:GetStringValue()
   sNvcUsd  = msNvcUsd:GetStringValue()
   iUSD     = tonumber(sNvc) * tonumber(sNvcUsd)
   print("Balance: ".. sNvc .. " * $" .. sNvcUsd .. " = " .. iUSD)

   -- Set some variables in the skin with the values we have created.
   SKIN:Bang("!SetVariable USD "..iUSD)
   --msUsdBalance:

   return tostring(iUSD)

end -- function Update